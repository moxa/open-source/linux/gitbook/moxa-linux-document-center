module.exports = {
  releasenote: [
    'MIL/what-is-MIL',
    {
      type: 'category',
      collapsed: false,
      label: 'Release Note',
      items: [
        {
          type: 'category',
          label: 'MIL 1.1.0 (Debian 9)',
          collapsed: false,
          items: [
            'MIL/MIL110/MIL110_UC-2100_UC-2100-W_ReleaseNote',
            'MIL/MIL110/MIL110_UC-3100_ReleaseNote',
            'MIL/MIL110/MIL110_UC-5100_ReleaseNote',
            'MIL/MIL110/MIL110_UC-8100_ReleaseNote',
            'MIL/MIL110/MIL110_UC-8100-ME-T_ReleaseNote',
            'MIL/MIL110/MIL110_UC-8100A-ME-T_ReleaseNote',
            'MIL/MIL110/MIL110_UC-8410A_ReleaseNote',
          ],
        },
        {
          type: 'category',
          label: 'MIL 1.1.1 (Debian 9)',
          collapsed: false,
          items: [
            'MIL/MIL111/MIL111_UC-2100_UC-2100-W_ReleaseNote',
            'MIL/MIL111/MIL111_UC-3100_ReleaseNote',
            'MIL/MIL111/MIL111_UC-8200_ReleaseNote',
          ],
        },
        {
          type: 'category',
          label: 'MIL 1.2.0 (Debian 9)',
          collapsed: false,
          items: [
            'MIL/MIL120/MIL120_UC-2100_UC-2100-W_ReleaseNote',
            'MIL/MIL120/MIL120_UC-3100_ReleaseNote',
            'MIL/MIL120/MIL120_UC-5100_ReleaseNote',
            'MIL/MIL120/MIL120_UC-8100_ReleaseNote',
            'MIL/MIL120/MIL120_UC-8100-ME-T_ReleaseNote',
            'MIL/MIL120/MIL120_UC-8100A-ME-T_ReleaseNote',
            'MIL/MIL120/MIL120_UC-8200_ReleaseNote',
            'MIL/MIL120/MIL120_UC-8410A_ReleaseNote',
            'MIL/MIL120/MIL120_UC-8540_ReleaseNote',
            'MIL/MIL120/MIL120_UC-8580_ReleaseNote',
          ],
        },
        {
          type: 'category',
          label: 'MIL 2.1.1 (Debian 10)',
          collapsed: false,
          items: [
            'MIL/MIL211/MIL211_V2406C-WL_ReleaseNote',
          ],
        },
         {
          type: 'category',
          label: 'MIL 3.0.0 (Debian 11)',
          collapsed: false,
          items: [
            'MIL/MIL300/MIL300_UC-8200_ReleaseNote',
          ],
        },
      ],
    },
    {
      type: 'category',
      collapsed: false,
      label: 'User Documentation',
      items: [
        {
          type: 'link',
          label: 'Moxa Connection Management (MCM) Reference Manual',
          href: 'https://moxa.gitlab.io/open-source/linux/gitbook/moxa-connection-manager-reference-manual/',
        },
      ],
    },
  ],
  manuals: [
    'manuals/what-is-x86-Linux-drivers-SDK',
    {
      type: 'category',
      label: 'x86 Linux drivers & SDK',
      collapsed: false,
      items: [
        'manuals/intro',
        {
          type: 'category',
          label: 'Drivers',
          collapsed: true,
          items: [
            'manuals/drivers/moxa-it87-gpio-driver',
            'manuals/drivers/moxa-it87-wdt-driver',
            'manuals/drivers/moxa-it87-serial-driver',
            'manuals/drivers/moxa-gpio-pca953x-driver',
            'manuals/drivers/moxa-hid-ft260-driver',
            'manuals/drivers/moxa-misc-driver',
            'manuals/drivers/moxa-irigb-driver',
            'manuals/drivers/moxa-hotswap-driver',
            'manuals/drivers/moxa-igc-driver',
            'manuals/drivers/moxa-mxu11x0-driver',
         ],
        },
        {
          type: 'category',
          label: 'Tools',
          collapsed: true,
          items: [
            'manuals/tools/mx-uart-ctl',
            'manuals/tools/mx-dio-ctl',
            'manuals/tools/mx-led-ctl',
            'manuals/tools/mx-relay-ctl',
            'manuals/tools/mx-module-ctl',
            'manuals/tools/mx-m2b-module-ctl',
            'manuals/tools/mx-power-igtd',
            'manuals/tools/moxa-hsrprp-tools',
            'manuals/tools/moxa-irigb-tools',
            'manuals/tools/moxa-audio-retask',
            'manuals/tools/moxa-drive-hotswapd',
            {
              type: 'category',
              label: 'MCU Tools',
              collapsed: true,
              items: [
              'manuals/tools/moxa-scaler-utils',
              'manuals/tools/moxa-app-wdt-utility',
              'manuals/tools/moxa-lpc-mcu-upgrade-tool',
              'manuals/tools/moxa-lan-bypass-utility',
              'manuals/tools/mx-expc-led-ctl',
             ],
            },
         ],
        },
        {
          type: 'category',
          label: 'Others',
          collapsed: true,
          items: [
            {
              type: 'category',
              label: 'Wireless',
              items: [
                {
                  type: 'category',
                  label: 'WiFi Module Usage',
                  items: [
                     'manuals/others/wireless/wifi/SparkLAN-WPEQ-261ANCI-BT'
                  ],
                },
                {
                  type: 'category',
                  label: 'Cellular Module Usage',
                  items: [
                     'manuals/others/wireless/cellular/UsingModemManager',
                     'manuals/others/wireless/cellular/Telit-LE910C4'
                  ],
                },
              ],
            },
            {
              type: 'category',
              label: 'Expansion Modules',
              items: [
                 'manuals/others/expansion_modules/DA_DN-PRP-HSR-I210',
                 'manuals/others/expansion_modules/DN-SP08-I-DB_TB',
                 'manuals/others/expansion_modules/IRIGB',
                 'manuals/others/expansion_modules/DE-PRP-HSR-EF',
                 'manuals/others/expansion_modules/DE-SP08-I-TB',
              ],
            },
            'manuals/others/linuxptp',
          ],
        },
        {
          type: 'category',
          label: 'Products Q&A',
          collapsed: true,
          items: [
            {
              type: 'category',
              label: 'DA series',
              items: [
                'manuals/products/DA/DA-820C',
                'manuals/products/DA/DA-682C',
                'manuals/products/DA/DA-681C',
                'manuals/products/DA/DA-680',
                'manuals/products/DA/DA-720',
              ]
            },
            {
              type: 'category',
              label: 'MPC series',
              items: [
                'manuals/products/MPC/MPC-2070',
                'manuals/products/MPC/MPC-2120',
              ]
            },
            {
              type: 'category',
              label: 'V series',
              items: [
                'manuals/products/V/V2403C',
                'manuals/products/V/V2406C',
                'manuals/products/V/V2201',
                'manuals/products/V/V3000',
              ]
            },
            {
              type: 'category',
              label: 'MC series',
              items: [
                'manuals/products/MC/MC-3201',
                'manuals/products/MC/MC-1100',
                'manuals/products/MC/MC-1200',
                'manuals/products/MC/MC-7400',
              ]
            },
            {
              type: 'category',
              label: 'EXPC series',
              items: [
                'manuals/products/EXPC/EXPC-F2000',
              ]
            },
          ],
        },
        {
          type: 'category',
          label: 'Release Note',
          collapsed: true,
          items: [
            'manuals/release_note/v1.1',
            'manuals/release_note/v1.0',
         ],
        },
      ],
    },
  ],
};
