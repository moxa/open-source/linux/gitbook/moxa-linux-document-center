---
title: UC-8540
---

## Moxa Industrial Linux(MIL) 1.2.0 for UC-8540 Series

| **System Image Version** | Build No. | Debian Ver. | Kernel Ver.                    | Release Date |
|:-------------------- | --------- | ----------- | ------------------------------ | ------------ |
| v2.2                | 22121316 | 9.13        | linux 4.4.302-cip70-rt40-moxa5+deb9 | 3/6/2023    |

## Table of contents

1. [Moxa Package Change Log](#moxa-package-change-log)
2. [Debian Software Package Change Log](#debian-software-package-change-log)
3. [Debian Security Patch](#debian-security-patch)
4. [Kernel Security Patch](#kernel-security-patch)

## Moxa Package Change Log

For detail change log of each package, refers to [UC-8540 MIL 1.2 Change Log](./changelog/MIL120_UC-8540_changelog_230117_165602.zip)

| Package                                          | Type    | Version                                            | Major Reason                                                                                                                          |
| ------------------------------------------------ | ------- | -------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------- |
|moxa-bootloader-manager|Add|'0.2.0+deb9'|Bootloader upgrade utility|
|uc8540-setinterface|Add|'1.3.0'|1. Correct parsing for root device node with storage is not mmcblk device when do partition extension. <br /> 2. Remove symbolic link for ttyMI due to modularized smartIO|"
|linux-headers-4.4.0-cip-rt-moxa-ls102xa|Upgrade|'4.4.285-cip63-rt36-moxa16+deb9' to '4.4.302-cip70-rt40-moxa5+deb9'|Update compiling tool for kernel version 4.4.302-cip70-rt40|
|linux-kbuild-4.4.0-cip-rt-moxa-ls102xa|Upgrade|'4.4.285-cip63-rt36-moxa16+deb9' to '4.4.302-cip70-rt40-moxa5+deb9'|Update compiling tool for kernel version 4.4.302-cip70-rt40|
|moxa-cellular-signald|Upgrade|'2.11.2' to '2.11.8'|Corrected cell_mgmt version number|
|moxa-cellular-utils|Upgrade|'2.11.2' to '2.11.8'|Corrected cell_mgmt version number|
|moxa-mil-base-system-armhf|Upgrade|'1.0.0+deb9u1' to '1.1.0+deb9'|Update MIL common package list|
|moxa-version|Upgrade|'1.2.0+deb9' to '1.3.3+deb9'|Fix CVE-2022-3088, which through a uccessful exploitation of the improper privilege management vulnerability could allow a local user with normal privileges to change their settings so they have root privileges on affected devices.|
|uc8540-base-system|Upgrade|'1.2.1' to '1.3.0'|Update UC-8540 package list|
|uc8540-kernel|Upgrade|'4.4.285-cip63-rt36-moxa16+deb9' to '4.4.302-cip70-rt40-moxa5+deb9'|Fix an issue in which the return value always shows failure even after successfully setting the watchdog timer flag WDIOS_ENABLECARD or WDIOS_DISABLECARD|
|uc8540-modules|Upgrade|'4.4.285-cip63-rt36-moxa16+deb9' to '4.4.302-cip70-rt40-moxa5+deb9'|Refers to uc8540-kernel|
|mxp-common-udev-rules|Upgrade|'1.1.0' to '1.1.1'|Removed unnecessary dependencies packages|












## Debian Software Package Change Log

For detail change log of each package, refers to [UC-8540 MIL 1.2 Change Log](./changelog/MIL120_UC-8540_changelog_230117_165602.zip)

| Package                                  | Type    | Version                                                           |
| ---------------------------------------- | ------- | ----------------------------------------------------------------- |
|firmware-misc-nonfree|Add|'20190114-2./~deb9u1'|
|mtd-utils|Add|'1:2.0.0-1'|
|apache2|Upgrade|'2.4.25-3+deb9u13' to '2.4.25-3+deb9u14'|
|apache2-bin|Upgrade|'2.4.25-3+deb9u13' to '2.4.25-3+deb9u14'|
|apache2-data|Upgrade|'2.4.25-3+deb9u13' to '2.4.25-3+deb9u14'|
|apache2-utils|Upgrade|'2.4.25-3+deb9u13' to '2.4.25-3+deb9u14'|
|curl|Upgrade|'7.52.1-5+deb9u16' to '7.52.1-5+deb9u17'|
|dbus|Upgrade|'1.10.32-0+deb9u1' to '1.10.32-0+deb9u2'|
|dpkg|Upgrade|'1.18.25' to '1.18.26'|
|gnupg|Upgrade|'2.1.18-8./~deb9u4' to '2.1.18-8./~deb9u5'|
|gnupg-agent|Upgrade|'2.1.18-8./~deb9u4' to '2.1.18-8./~deb9u5'|
|gpgv|Upgrade|'2.1.18-8./~deb9u4' to '2.1.18-8./~deb9u5'|
|gzip|Upgrade|'1.6-5+b1' to '1.6-5+deb9u1'|
|isc-dhcp-client|Upgrade|'4.3.5-3+deb9u2' to '4.3.5-3+deb9u3'|
|isc-dhcp-common|Upgrade|'4.3.5-3+deb9u2' to '4.3.5-3+deb9u3'|
|libc-bin|Upgrade|'2.24-11+deb9u4' to '2.24-11+deb9u5'|
|libc-dev-bin|Upgrade|'2.24-11+deb9u4' to '2.24-11+deb9u5'|
|libc-l10n|Upgrade|'2.24-11+deb9u4' to '2.24-11+deb9u5'|
|libc6|Upgrade|'2.24-11+deb9u4' to '2.24-11+deb9u5'|
|libc6-dev|Upgrade|'2.24-11+deb9u4' to '2.24-11+deb9u5'|
|libcurl3|Upgrade|'7.52.1-5+deb9u16' to '7.52.1-5+deb9u17'|
|libcurl3-gnutls|Upgrade|'7.52.1-5+deb9u16' to '7.52.1-5+deb9u17'|
|libcurl4-gnutls-dev|Upgrade|'7.52.1-5+deb9u16' to '7.52.1-5+deb9u17'|
|libdbus-1-3|Upgrade|'1.10.32-0+deb9u1' to '1.10.32-0+deb9u2'|
|libdns-export162|Upgrade|'1:9.10.3.dfsg.P4-12.3+deb9u12' to '1:9.10.3.dfsg.P4-12.3+deb9u13'|
|libexpat1|Upgrade|'2.2.0-2+deb9u5' to '2.2.0-2+deb9u7'|
|libexpat1-dev|Upgrade|'2.2.0-2+deb9u5' to '2.2.0-2+deb9u7'|
|libglib2.0-0|Upgrade|'2.50.3-2+deb9u2' to '2.50.3-2+deb9u4'|
|libisc-export160|Upgrade|'1:9.10.3.dfsg.P4-12.3+deb9u12' to '1:9.10.3.dfsg.P4-12.3+deb9u13'|
|libksba8|Upgrade|'1.3.5-2' to '1.3.5-2+deb9u1'|
|libldap-2.4-2|Upgrade|'2.4.44+dfsg-5+deb9u8' to '2.4.44+dfsg-5+deb9u9'|
|libldap-common|Upgrade|'2.4.44+dfsg-5+deb9u8' to '2.4.44+dfsg-5+deb9u9'|
|liblzma5|Upgrade|'5.2.2-1.2+b1' to '5.2.2-1.2+deb9u1'|
|libncurses5|Upgrade|'6.0+20161126-1+deb9u2' to '6.0+20161126-1+deb9u3'|
|libncursesw5|Upgrade|'6.0+20161126-1+deb9u2' to '6.0+20161126-1+deb9u3'|
|libpam-systemd|Upgrade|'232-25+deb9u13' to '232-25+deb9u15'|
|libsnmp-base|Upgrade|'5.7.3+dfsg-1.7+deb9u3' to '5.7.3+dfsg-1.7+deb9u4'|
|libsnmp30|Upgrade|'5.7.3+dfsg-1.7+deb9u3' to '5.7.3+dfsg-1.7+deb9u4'|
|libsqlite3-0|Upgrade|'3.16.2-5+deb9u3' to '3.16.2-5+deb9u4'|
|libssl1.1|Upgrade|'1.1.0l-1./~deb9u5' to '1.1.0l-1./~deb9u7'|
|libsystemd0|Upgrade|'232-25+deb9u13' to '232-25+deb9u15'|
|libtinfo5|Upgrade|'6.0+20161126-1+deb9u2' to '6.0+20161126-1+deb9u3'|
|libudev1|Upgrade|'232-25+deb9u13' to '232-25+deb9u15'|
|libxml2|Upgrade|'2.9.4+dfsg1-2.2+deb9u6' to '2.9.4+dfsg1-2.2+deb9u9'|
|linux-libc-dev|Upgrade|'4.9.320-2' to '4.9.320-3'|
|locales|Upgrade|'2.24-11+deb9u4' to '2.24-11+deb9u5'|
|multiarch-support|Upgrade|'2.24-11+deb9u4' to '2.24-11+deb9u5'|
|ncurses-base|Upgrade|'6.0+20161126-1+deb9u2' to '6.0+20161126-1+deb9u3'|
|ncurses-bin|Upgrade|'6.0+20161126-1+deb9u2' to '6.0+20161126-1+deb9u3'|
|openssl|Upgrade|'1.1.0l-1./~deb9u5' to '1.1.0l-1./~deb9u7'|
|rsyslog|Upgrade|'8.24.0-1+deb9u1' to '8.24.0-1+deb9u3'|
|snmp|Upgrade|'5.7.3+dfsg-1.7+deb9u3' to '5.7.3+dfsg-1.7+deb9u4'|
|snmpd|Upgrade|'5.7.3+dfsg-1.7+deb9u3' to '5.7.3+dfsg-1.7+deb9u4'|
|sudo|Upgrade|'1.8.19p1-2.1+deb9u3' to '1.8.19p1-2.1+deb9u4'|
|systemd|Upgrade|'232-25+deb9u13' to '232-25+deb9u15'|
|systemd-sysv|Upgrade|'232-25+deb9u13' to '232-25+deb9u15'|
|tzdata|Upgrade|'2021a-0+deb9u3' to '2021a-0+deb9u7'|
|udev|Upgrade|'232-25+deb9u13' to '232-25+deb9u15'|
|vim|Upgrade|'2:8.0.0197-4+deb9u5' to '2:8.0.0197-4+deb9u7'|
|vim-common|Upgrade|'2:8.0.0197-4+deb9u5' to '2:8.0.0197-4+deb9u7'|
|vim-runtime|Upgrade|'2:8.0.0197-4+deb9u5' to '2:8.0.0197-4+deb9u7'|
|wireless-regdb|Upgrade|'2016.06.10-1' to '2022.04.08-1./~deb9u1'|
|xxd|Upgrade|'2:8.0.0197-4+deb9u5' to '2:8.0.0197-4+deb9u7'|
|zlib1g|Upgrade|'1:1.2.8.dfsg-5+deb9u1' to '1:1.2.8.dfsg-5+deb9u2'|








## Debian Security Patch

Refers to [UC-8540 MIL 1.2 Change Log](./changelog/MIL120_UC-8540_changelog_230117_165602.zip)

## 

## Kernel Security Patch

Refer to below table for the list of kernel security patch

|                  |                |
| ---------------- | -------------- |
|	CVE-2020-29374	|	CVE-2022-1011	|
|	CVE-2020-36516	|	CVE-2022-1016	|
|	CVE-2021-20321	|	CVE-2022-1198	|
|	CVE-2021-26401	|	CVE-2022-1353	|
|	CVE-2021-28711	|	CVE-2022-1516	|
|	CVE-2021-28712	|	CVE-2022-1652	|
|	CVE-2021-28713	|	CVE-2022-1729	|
|	CVE-2021-28715	|	CVE-2022-1734	|
|	CVE-2021-37159	|	CVE-2022-1836	|
|	CVE-2021-3752	|	CVE-2022-1974	|
|	CVE-2021-3760	|	CVE-2022-1975	|
|	CVE-2021-3896	|	CVE-2022-20368	|
|	CVE-2021-39685	|	CVE-2022-20423	|
|	CVE-2021-4002	|	CVE-2022-2380	|
|	CVE-2021-4034	|	CVE-2022-24958	|
|	CVE-2021-4083	|	CVE-2022-25258	|
|	CVE-2021-4149	|	CVE-2022-25375	|
|	CVE-2021-4155	|	CVE-2022-2639	|
|	CVE-2021-4202	|	CVE-2022-26490	|
|	CVE-2021-4203	|	CVE-2022-26966	|
|	CVE-2021-43389	|	CVE-2022-27223	|
|	CVE-2021-43976	|	CVE-2022-28356	|
|	CVE-2021-45095	|	CVE-2022-28390	|
|	CVE-2021-45868	|	CVE-2022-2964	|
|	CVE-2022-0001	|	CVE-2022-30594	|
|	CVE-2022-0002	|	CVE-2022-3202	|
|	CVE-2022-0330	|	CVE-2022-32296	|
|	CVE-2022-0487	|	CVE-2022-32981	|
|	CVE-2022-0494	|	CVE-2022-33981	|
|	CVE-2022-0617	|	CVE-2022-41858	|
