---
title: UC-2100/UC-2100-W
---
## Moxa Industrial Linux(MIL) 1.1.1 for UC-2100 and UC-2100-W Series

| **System Image Version** | Build No. | Debian Ver. | Kernel Ver.                    | Release Date |
|:-------------------- | --------- | ----------- | ------------------------------ | ------------ |
| v1.12                | 22030117  | 9.13        | linux 4.4.285-cip63-rt36-moxa11+deb9 | 3/1/2023    |

## Table of contents

1. [Moxa Package Change Log](#moxa-package-change-log)
2. [Debian Software Package Change Log](#debian-software-package-change-log)
3. [Debian Security Patch](#debian-security-patch)
4. [Kernel Security Patch](#kernel-security-patch)

## Moxa Package Change Log


| Package                                          | Type    | Version                                            | Major Reason                                                                                                                          |
| ------------------------------------------------ | ------- | -------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------- |
|linux-headers-4.4.0-cip-rt-moxa-am335x|Upgrade|'4.4.285-cip63-rt36-moxa8+deb9' to '4.4.285-cip63-rt36-moxa13+deb9'|Update compiling tool for kernel version 4.4.285-cip63-rt36|
|linux-kbuild-4.4.0-cip-rt-moxa-am335x|Upgrade|'4.4.285-cip63-rt36-moxa8+deb9' to '4.4.285-cip63-rt36-moxa13+deb9'|Update compiling tool for kernel version 4.4.285-cip63-rt36|
|moxa-cellular-signald|Upgrade|'2.11.1' to '2.11.2'|Support additional UC Models|
|moxa-cellular-utils|Upgrade|'2.11.1' to '2.11.2'|Support additional UC Models|
|sparklan-qca9377-driver-4.4.0-cip-rt-moxa-am335x|Upgrade|'4.4.285-cip63-rt36-moxa8+deb9' to '4.4.285-cip63-rt36-moxa11+deb9'|Update the dependent kernel version to 4.4.285-cip63-rt36|
|uc2100-base-system|Upgrade|'1.7.1' to '1.8.0'|Update UC-2100 package list|
|uc2100-kernel|Upgrade|'4.4.285-cip63-rt36-moxa8+deb9' to '4.4.285-cip63-rt36-moxa13+deb9'|Support IC+ Ethernet PHY|
|uc2100-modules-std|Upgrade|'4.4.285-cip63-rt36-moxa8+deb9' to '4.4.285-cip63-rt36-moxa13+deb9'|Support IC+ Ethernet PHY|



## Debian Software Package Change Log

There are no updates in this image version v1.12


## Debian Security Patch

There are no updates in this image version v1.12

## 

## Kernel Security Patch

There are no updates in this image version v1.12
