---
title: moxa-misc-driver
---

## Moxa Misc Driver

The Moxa misc driver supported to access the relay/DI/DO,
on board uart interface and programable LED control on DA-720/DA-682B,
DA-682B-K6 MC-1100, MC-7400, MP-2070/2120/2121, ... series embedded computer.

The driver is based on GPIO sysfs, the Linux kernel shuld support thses features, CONFIG_GPIO_SYSFS=y and CONFIG_GPIOLIB=y.

### Support Models

- DA Series:
  - [DA-720](../products/DA/DA-720.md)
- MPC Series:
  - [MPC-2070](../products/MPC/MPC-2070.md)
  - [MPC-2120](../products/MPC/MPC-2120.md)
- MC Serise:
  - [MC-1100](../products/MC/MC-1100.md)
  - [MC-7400](../products/MC/MC-7400.md)

### Source Code Link

[moxa-misc-driver](https://gitlab.com/moxa/open-source/linux/packages/moxa-misc-drivers/-/tree/main)

### What Tools Depend On?

N/A

### Usage (Ubuntu 20.04/Debian 11)

- Install required packages

```
apt update
apt install --no-install-recommends -qqy build-essential
apt install --no-install-recommends -qqy linux-headers-$(uname -r)
```

:::info
You should configure the CONFIG_PRODUCT before building this driver in Makefile.
:::
- Build kernel module
  1. Run `make` to build kernel module
  2. Once build successful, `misc-moxa-<model_name>.ko` could be found under current directory
  3. Run `make install` to install kernel module on `/usr/lib/modules/$(uname -r)/`

### Usage (CentOS 7.9)
- Sync the latest version available from any enabled repository and reboot system

```
yum distro-sync
reboot
```

- Install required packages

```
yum install "kernel-devel-$(uname -r)"
yum install "kernel-headers-$(uname -r)"
yum groupinstall "Development Tools"
```
:::info
You should configure the CONFIG_PRODUCT before building this driver in Makefile.
:::
- Build kernel module
  1. Run `make` to build kernel module
  2. Once build successful, `misc-moxa-<model_name>.ko` could be found under current directory
  3. Run `make install` to install kernel module on `/usr/lib/modules/$(uname -r)/`

:::note
CentOS 7 latest kernel moved from "kernel.ko" to "kernel.ko.xz", please ensure target "kernel.ko.xz" is removed or moved as a backup.
:::

### More Information

See Readme in [moxa-misc-driver](https://gitlab.com/moxa/open-source/linux/packages/moxa-misc-drivers/-/tree/main)
