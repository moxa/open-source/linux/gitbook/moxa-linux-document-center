---
title: moxa-hotswap-driver
---

## Moxa HDD/SSD hotswap driver

`moxa-hotswap-driver`: The hotswap driver is for moxa V2616A embedded computer. /dev/hotswap is the entry point for tracking the disks hotswapping. You can use the 'mxhtspd' to monitor and programming the button and harddisk hotswap. The mxhtspd provides a callback script for stopping your program and hotswapping your disk in run time. Following describes how to compile this driver.

### Support Models
- V Series:
  - [V2406C](../products/V/V2406C.md)

### Source Code Link

[moxa-hotswap-driver](https://gitlab.com/moxa/open-source/linux/packages/moxa-hotswap-driver)

### What Tools Depend On?

- [Moxa Drive Hotswapd](https://gitlab.com/moxa/open-source/linux/packages/moxa-drive-hotswapd)