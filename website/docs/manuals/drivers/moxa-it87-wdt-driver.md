---
title: moxa-it87-wdt-driver
---

### Moxa IT87 Watchdog Timer Driver

`moxa-it87-wdt-driver`: Watchdog Timer Driver for ITE IT87xx Environment Control


#### Support Models

- DA Series:
  - [DA-820C](../products/DA/DA-820C.md)
  - [DA-682C](../products/DA/DA-682C.md)
  - [DA-681C](../products/DA/DA-681C.md)
  - [DA-680](../products/DA/DA-680.md)
  - [DA-720](../products/DA/DA-720.md)
- MC Series:
  - [MC-3201](../products/MC/MC-3201.md)
  - [MC-1100](../products/MC/MC-1100.md)
  - [MC-1200](../products/MC/MC-1200.md)
  - [MC-7400](../products/MC/MC-7400.md)
- V Series:
  - [V2403C](../products/V/V2403C.md)
  - [V2201](../products/V/V2201.md)
  - [V3000](../products/V/V3000.md)
- EXPC Series:
  - [EXPC-F2000](../products/EXPC/EXPC-F2000.md)

#### Source Code Link

- For Kernel Version >= 5.x
  - [moxa-it87-wdt-driver (5.x)](https://gitlab.com/moxa/open-source/linux/packages/moxa-it87-wdt-driver/-/tree/5.2/master)

- For Kernel Version >= 4.x
  - [moxa-it87-wdt-driver (4.x)](https://gitlab.com/moxa/open-source/linux/packages/moxa-it87-wdt-driver/-/tree/4.9.x/master)

:::info
Please install [moxa-it87-wdt-driver **(5.x)**](https://gitlab.com/moxa/open-source/linux/packages/moxa-it87-wdt-driver/-/tree/5.2/master) on CentOS 7.9
:::

### What Tools Depend On?

Watchdog deamon from Linux Distributions

- Debian: https://packages.debian.org/bullseye/watchdog
- Ubuntu: https://manpages.ubuntu.com/manpages/focal/man8/watchdog.8.html
- CentOS 7: https://centos.pkgs.org/7/centos-x86_64/watchdog-5.13-12.el7.x86_64.rpm.html

### Usage (Ubuntu 20.04/Debian 11)

- Install required packages

```
apt update
apt install --no-install-recommends -qqy build-essential
apt install --no-install-recommends -qqy linux-headers-$(uname -r)
```

- Build kernel module
  1. Run `make` to build kernel module
  2. Once build successful, `it87_wdt.ko` could be found under current directory
  3. Run `make install` to install kernel module on `/usr/lib/modules/$(uname -r)/`

### Usage (CentOS 7.9)

- Sync the latest version available from any enabled repository and reboot system
```
yum distro-sync
reboot
```

- Install required packages
```
yum install "kernel-devel-$(uname -r)"
yum install "kernel-headers-$(uname -r)"
yum groupinstall "Development Tools"
```

- Build kernel module
  1. Run `make` to build kernel module
  2. Once build successful, `it87_wdt.ko` could be found under current directory
  3. Run `make install` to install kernel module on `/usr/lib/modules/$(uname -r)/`

:::note
CentOS 7 latest kernel moved from "kernel.ko" to "kernel.ko.xz", please ensure target "kernel.ko.xz" is removed or moved as a backup.
:::

### Probing `it87_wdt` driver on boot.
1. Add configuration file.

- Debian/CentOS
  Edit `/lib/modprobe.d/watchdog.conf`

- Ubuntu
  Edit `/etc/modprobe.d/watchdog.conf`

```
# timeout:Watchdog timeout in seconds, default=60 (int)
# nowayout:Watchdog cannot be stopped once started, default=0 (bool)
options it87_wdt nowayout=1 timeout=60
```

:::caution
`V3000` and `EXPC-F2000` platform should default disable watchdog `KRST` reset pin
1. Upgrade `it87_wdt` driver to `1.4.0` or later version
2. Add `krst=0` option into onfiguration file: 
```
# timeout: Watchdog timeout in seconds, default=60 (int)
# nowayout: Watchdog cannot be stopped once started, default=0 (bool)
# krst: Watchdog enable KRST reset output, default=1 (bool)
options it87_wdt nowayout=1 timeout=60 krst=0
```
:::

2. Disable `iTCO_wdt` driver.

  Edit `/lib/modprobe.d/iTCO-blacklist.conf`
  ```
  blacklist iTCO_wdt
  blacklist iTCO_vendor_support
  ```

### Disable `NMI watchdog` driver (Ubuntu)

Edit the `/etc/default/grub` file as root and then add nmi_watchdog=0 to the line starting with GRUB_CMDLINE_LINUX_DEFAULT
```
GRUB_CMDLINE_LINUX_DEFAULT="nmi_watchdog=0"
```
After adding the kernel parameter, don't forget to update the GRUB
```
sudo update-grub
```

### Watchdog daemon package (Debian/Ubuntu)
1. Install package
  ```
  sudo apt-get update
  sudo apt-get install watchdog
  ```
2. Edit `/etc/default/watchdog`
  ```
  # Start watchdog at boot time? 0 or 1
  run_watchdog=1
  # Start wd_keepalive after stopping watchdog? 0 or 1
  run_wd_keepalive=0
  # Load module before starting watchdog
  watchdog_module="it87_wdt"
  # Specify additional watchdog options here (see manpage).
  ```
3. Add device node to config, edit `/etc/watchdog.conf`
  ```
  watchdog-device = /dev/watchdog
  ```

4. Troubleshooting for `V3000` platform
:::caution
`V3000` platform super IO gpio value maybe changed when probing `it87_wdt` driver somehow,
please using `/usr/sbin/mx-uart-ctl` to fix the issue in watchdog service file. See [mx-uart-ctl](../tools/mx-uart-ctl.md).
:::

  Edit `/lib/systemd/system/watchdog.service` file,
  and add this line `ExecStartPre=/bin/bash -c '/usr/sbin/mx-uart-ctl -p 0 && /usr/sbin/mx-uart-ctl -p 1'` in [Service] section.
  ```
  [Unit]
  Description=watchdog daemon
  Conflicts=wd_keepalive.service
  After=multi-user.target
  OnFailure=wd_keepalive.service
  
  [Service]
  ExecStartPre=/bin/bash -c '/usr/sbin/mx-uart-ctl -p 0 && /usr/sbin/mx-uart-ctl -p 1'
  Type=forking
  EnvironmentFile=/etc/default/watchdog
  ExecStartPre=/bin/sh -c '[ -z "${watchdog_module}" ] || [ "${watchdog_module}" = "none" ] || /sbin/modprobe $watchdog_module'
  ExecStart=/bin/sh -c '[ $run_watchdog != 1 ] || exec /usr/sbin/watchdog $watchdog_options'
  ExecStopPost=/bin/sh -c '[ $run_wd_keepalive != 1 ] || false'
  
  [Install]
  WantedBy=default.target
  ```
