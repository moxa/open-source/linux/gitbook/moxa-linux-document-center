---
title: moxa-igc-driver
---

## Moxa IGC Driver

The igc driver is for Intel(R) I225-LM/I225-V 2.5G Ethernet Controller

- For **Debian 11 (kernel 5.10)**, must use this driver for 2.5G ethernet controller
- For **Ubuntu 22.04 (kernel 5.15)**, to use Ubuntu's default igc driver, please skip this step

### Support Models

- V Series: [V3000](../products/V/V3000.md)

### Source Code Link

[moxa-igc-driver](https://gitlab.com/moxa/open-source/linux/packages/moxa-igc-driver/-/tree/master)

### Usage (Debian 11)

- Install required packages

```
apt update && apt install build-essential linux-headers-`uname -r` git
```

- Build kernel module
```
git clone https://gitlab.com/moxa/open-source/linux/packages/moxa-igc-driver.git
cd moxa-igc-driver
make -C /lib/modules/`uname -r`/build M=$PWD
# backup original igc driver
cp /lib/modules/`uname -r`/kernel/drivers/net/ethernet/intel/igc/igc.ko /lib/modules/`uname -r`/kernel/drivers/net/ethernet/intel/igc/igc.ko.bak
cp igc.ko /lib/modules/`uname -r`/kernel/drivers/net/ethernet/intel/igc/igc.ko
depmod -a
```

- Check igc driver status after re-probe modules
```
# dmesg | grep igc
[    1.314825] igc: loading out-of-tree module taints kernel.
[    1.314869] igc: module verification failed: signature and/or required key missing - tainting kernel
[    1.365085] igc 0000:07:00.0 (unnamed net_device) (uninitialized): PHC added
[    1.425072] igc 0000:07:00.0: 4.000 Gb/s available PCIe bandwidth (5.0 GT/s PCIe x1 link)
[    1.425076] igc 0000:07:00.0 eth3: MAC: 00:90:e8:00:00:17
[    1.953092] igc 0000:07:00.0 enp7s0: renamed from eth3

# modinfo igc
filename:       /lib/modules/5.10.0-12-amd64/kernel/drivers/net/ethernet/intel/igc/igc.ko
license:        GPL v2
description:    Intel(R) 2.5G Ethernet Linux Driver
author:         Intel Corporation, <linux.nics@intel.com>
alias:          pci:v00008086d000015FDsv*sd*bc*sc*i*
alias:          pci:v00008086d0000125Fsv*sd*bc*sc*i*
alias:          pci:v00008086d0000125Esv*sd*bc*sc*i*
alias:          pci:v00008086d0000125Dsv*sd*bc*sc*i*
alias:          pci:v00008086d0000125Csv*sd*bc*sc*i*
alias:          pci:v00008086d0000125Bsv*sd*bc*sc*i*
alias:          pci:v00008086d00000D9Fsv*sd*bc*sc*i*
alias:          pci:v00008086d00005502sv*sd*bc*sc*i*
alias:          pci:v00008086d00003101sv*sd*bc*sc*i*
alias:          pci:v00008086d00003100sv*sd*bc*sc*i*
alias:          pci:v00008086d000015F7sv*sd*bc*sc*i*
alias:          pci:v00008086d000015F8sv*sd*bc*sc*i*
alias:          pci:v00008086d000015F3sv*sd*bc*sc*i*
alias:          pci:v00008086d000015F2sv*sd*bc*sc*i*
depends:        ptp
retpoline:      Y
name:           igc
vermagic:       5.10.0-12-amd64 SMP mod_unload modversions
parm:           debug:Debug level (0=none,...,16=all) (int)
```
