---
title: moxa-it87-gpio-driver
---

## Moxa IT87 GPIO Driver

`moxa-it87-gpio-driver`: GPIO interface for IT87xx Super I/O chips

### Support Models

- DA Series:
  - [DA-820C](../products/DA/DA-820C.md)
  - [DA-682C](../products/DA/DA-682C.md)
  - [DA-681C](../products/DA/DA-681C.md)
  - [DA-680](../products/DA/DA-680.md)
- MC Series:
  - [MC-3201](../products/MC/MC-3201.md)
  - [MC-1200](../products/MC/MC-1200.md)
- V Series:
  - [V2403C](../products/V/V2403C.md)
  - [V2201](../products/V/V2201.md)
  - [V3000](../products/V/V3000.md)

### Source Code Link

[moxa-it87-gpio-driver](https://gitlab.com/moxa/open-source/linux/packages/moxa-it87-gpio-driver/-/tree/master)

### What Tools Depend On?

- [mx-uart-ctl](../tools/mx-uart-ctl.md)
- [mx-dio-ctl](../tools/mx-dio-ctl.md)
- [mx-led-ctl](../tools/mx-led-ctl.md)
- [mx-relay-ctl](../tools/mx-relay-ctl.md)
- [mx-module-ctl](../tools/mx-module-ctl.md)

### Usage (Ubuntu 20.04/Debian 11)

- Install required packages

```
apt update
apt install --no-install-recommends -qqy build-essential dmidecode
apt install --no-install-recommends -qqy linux-headers-$(uname -r)
```

- Build kernel module
  1. Run `make` to build kernel module
  2. Once build successful, `gpio-it87.ko` could be found under current directory
  3. Run `make install` to install kernel module on `/usr/lib/modules/$(uname -r)/`

### Usage (CentOS 7.9)

- Sync the latest version available from any enabled repository and reboot system
```
yum distro-sync
reboot
```

- Install required packages

```
yum install "kernel-devel-$(uname -r)"
yum install "kernel-headers-$(uname -r)"
yum install dmidecode
yum groupinstall "Development Tools"
```

- Build kernel module
  1. Run `make` to build kernel module
  2. Once build successful, `gpio-it87.ko` could be found under current directory
  3. Run `make install` to install kernel module on `/usr/lib/modules/$(uname -r)/`
  4. Reboot the system

:::note
CentOS 7 latest kernel moved from "kernel.ko" to "kernel.ko.xz", please ensure target "kernel.ko.xz" is removed or moved as a backup.
:::

### Troubleshooting (CentOS 7.9)

- Device or resource busy when load gpio-it87 driver
  - Add `acpi_enforce_resources=lax` on boot parameter
  ```
  # Edit grub config
  vi /etc/default/grub
  GRUB_CMDLINE_LINUX="[...] acpi_enforce_resources=lax"

  # If you change this file, run 'grub2-mkconfig' afterwards to update
  grub2-mkconfig -o /etc/grub2.cfg
  grub2-mkconfig -o /etc/grub2-efi.cfg
  ```

- Enforce to use it87 series driver instead of iTCO driver (if necessary)
```
# edit `/lib/modprobe.d/iTCO-blacklist.conf`

blacklist iTCO_wdt
blacklist iTCO_vendor_support
```
