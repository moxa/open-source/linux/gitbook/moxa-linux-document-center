---
title: What is x86 Linux drivers & SDK
slug: /manuals/
---

## Introduction

**x86 Linux drivers & SDK** provides Moxa Linux x86 computer develop user guide, 
including peripheral drivers porting guide, necessary setting up on Linux distributions, 
driver porting, I/O controlling example code, etc.

### I/O Driver User Guide

**I/O Driver User Guide** provides for I/O drivers user guide, including how to build on Debian, Ubuntu and CentOS.

### Tools User Guide

**Tools User Guide** provides peripheral controlling tools user guide, and common example code for controlling peripheral on specific platforms.

### Others
#### Wireless Module Driver User Guide

**Wireless Module Driver User Guide** provides RF modules user guide for cellular and WiFi modules, 
including dial-up steps, troubleshooting, and drivers porting guide (if necessary).

### Release Note

**Release Note** describes the release note of the package of drivers/tools/products.

### Prodcucts Q&A

**Prodcucts Q&A** provides more detail information like troubleshooting for specific products.
