---
title: MC-1100
---

## Datasheet

- [MC-1100 Series Website](https://www.moxa.com/en/products/industrial-computing/x86-computers/mc-1100-series)
- [moxa-MC-1100-series-datasheet-v1.5.pdf](https://cdn-cms.azureedge.net/getmedia/31cfe0f9-deea-4f44-aabb-0360d60c8c6a/moxa-mc-1100-series-datasheet-v1.3.pdf)

## Prerequisite
- Build [moxa-misc-driver](../../drivers/moxa-misc-driver.md) on MC-1100 host.

## Troubleshooting

1. failed to load firmware

```
[    7.529638] i915 0000:00:02.0: firmware: failed to load i915/skl_dmc_ver1_27.bin (-2)
[    7.529644] firmware_class: See https://wiki.debian.org/Firmware for information about missing firmware
[    7.889522] r8169 0000:10:00.0: firmware: failed to load rtl_nic/rtl8168h-2.fw (-2)
[    7.889532] r8169 0000:10:00.0: Unable to load firmware rtl_nic/rtl8168h-2.fw (-2)
```

On Debian 11/Ubuntu 20.04:

```
# edit /etc/apt/source.list, and append 'non-free' to list,
# your source.list should look like this:
deb http://deb.debian.org/debian/ bullseye main non-free

# and run
sudo apt-get update
sudo apt-get install firmware-realtek firmware-misc-nonfree
```