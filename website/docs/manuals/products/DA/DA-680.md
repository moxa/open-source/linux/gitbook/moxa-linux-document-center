---
title: DA-680
---

## Datasheet
TBD

## Prerequisite

See [Introduction](../../intro.md) page to check necessary drivers/tools/modules informations.

## Troubleshooting

1. Failed to load firmware

```
[    7.529638] i915 0000:00:02.0: firmware: failed to load i915/skl_dmc_ver1_27.bin (-2)
[    7.529644] firmware_class: See https://wiki.debian.org/Firmware for information about missing firmware
[    7.889522] r8169 0000:10:00.0: firmware: failed to load rtl_nic/rtl8168h-2.fw (-2)
[    7.889532] r8169 0000:10:00.0: Unable to load firmware rtl_nic/rtl8168h-2.fw (-2)
```
On Debian 11:
```
# edit /etc/apt/source.list, and append 'non-free' to list,
# your source.list should look like this:
deb http://deb.debian.org/debian/ bullseye main non-free

# and run
sudo apt-get update
sudo apt-get install firmware-realtek firmware-misc-nonfree
```

On Ubuntu 20.04:
```
sudo apt-get update
sudo apt-get install linux-firmware