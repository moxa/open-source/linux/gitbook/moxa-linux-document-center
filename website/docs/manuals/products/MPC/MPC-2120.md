---
title: MPC-2120
---

## Datasheet

- [MPC-2120 Series Website](https://www.moxa.com/en/products/industrial-computing/panel-computers-and-displays/mpc-2120-series)
- [moxa-mpc-2120-series-datasheet-v1.6.pdf](https://cdn-cms.azureedge.net/getmedia/7c256eb4-a7d0-49db-8e56-ae02a5978e79/moxa-mpc-2120-series-datasheet-v1.6.pdf)

## Prerequisite
- Build [moxa-misc-driver](../../drivers/moxa-misc-driver.md) on MPC-2120 host.

## Support Linux Distro
- Debian 11 with GNOME desktop environment

## Troubleshooting

1. failed to load firmware

```
[    7.529638] i915 0000:00:02.0: firmware: failed to load i915/skl_dmc_ver1_27.bin (-2)
[    7.529644] firmware_class: See https://wiki.debian.org/Firmware for information about missing firmware
[    7.889522] r8169 0000:10:00.0: firmware: failed to load rtl_nic/rtl8168h-2.fw (-2)
[    7.889532] r8169 0000:10:00.0: Unable to load firmware rtl_nic/rtl8168h-2.fw (-2)
```

On Debian 11/Ubuntu 20.04:

```
# edit /etc/apt/source.list, and append 'non-free' to list,
# your source.list should look like this:
deb http://deb.debian.org/debian/ bullseye main non-free

# and run
sudo apt-get update
sudo apt-get install firmware-realtek firmware-misc-nonfree
```