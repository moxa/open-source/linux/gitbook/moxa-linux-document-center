---
title: V2201
---

### Datasheet
- [V2201 Series Website](https://www.moxa.com/en/products/industrial-computing/x86-computers/v2201-series)
- [moxa-v2201-series-datasheet-v2.8.pdf](https://cdn-cms.azureedge.net/getmedia/6681a025-83b9-421c-8e8d-538103e295ab/moxa-v2201-series-datasheet-v2.8.pdf)

### Prerequisite
See [Introduction](../../intro.md) page to check necessary drivers/tools/modules informations.

### Troubleshooting

1. Failed to load firmware

```
[    7.529638] i915 0000:00:02.0: firmware: failed to load i915/skl_dmc_ver1_27.bin (-2)
[    7.529644] firmware_class: See https://wiki.debian.org/Firmware for information about missing firmware
[    7.889522] r8169 0000:10:00.0: firmware: failed to load rtl_nic/rtl8168h-2.fw (-2)
[    7.889532] r8169 0000:10:00.0: Unable to load firmware rtl_nic/rtl8168h-2.fw (-2)
```

2. On Debian 11/Ubuntu 20.04:

```
# edit /etc/apt/source.list, and append 'non-free' to list,
# your source.list should look like this:
deb http://deb.debian.org/debian/ bullseye main non-free

# and run
sudo apt-get update
sudo apt-get install firmware-realtek firmware-misc-nonfree
```