---
title: mx-uart-ctl
---

### Moxa UART Control

`mx-uart-ctl` is a common script for controlling UART's interface mode

### Source Code Link
[Download](https://gitlab.com/moxa/open-source/linux/packages/moxa-example-code/-/blob/main/x86/UART/mx-uart-ctl)

### Support Model

- DA Series:
  - [DA-820C](#da-820c)
  - [DA-682C](#da-682c)
  - [DA-681C](#da-681c)
  - [DA-680](#da-680)
- MC Series:
  - [MC-1200](#mc-1200)
- V Series:
  - [V2403C](#v2403c)
  - [V2201](#v2201)
  - [V3000](#V3000)
- EXPC Series
  - [EXPC-F2000](#expc-f2000)


### Prerequisite
Before use `mx-uart-ctl` script, you should install drivers as below.
1. [moxa-it87-gpio-driver](../drivers/moxa-it87-gpio-driver.md)
2. [moxa-it87-serial-driver](../drivers/moxa-it87-serial-driver.md)

For **DA-820C/DA-682C/DA-681C** models, the following drivers also need to be loaded.
1. [moxa-gpio-pca953x-driver](../drivers/moxa-gpio-pca953x-driver.md)
2. [moxa-hid-ft260-driver](../drivers/moxa-hid-ft260-driver.md)

For **EXPC-F2000** models, only the following driver needs to be loaded.
1. [moxa-gpio-pca953x-driver](../drivers/moxa-gpio-pca953x-driver.md)

For **DA-680** model, the following driver and utility need to be loaded and installed.
1. [moxa-mxu11x0-driver](../drivers/moxa-mxu11x0-driver.md)
2. Install **setserial**
    ```
    # Install package on Ubuntu 20.04 or Debian 11
    apt install setserial -y

    # Install package on CentOS 7.9
    yum install setserial
    ```
  - Due to TUSB3410 chips are partially connected to two USB hubs, the ttyUSB ports number are not increased correctly from driver.
  Add udev rules to create **ttyM** symlinks: edit **/lib/udev/rules.d/10-moxa-serial.rules**
  ```
  KERNEL=="ttyUSB[0-9]*", KERNELS=="1-7.1:1.0", SUBSYSTEM=="tty", ACTION=="add", SYMLINK+="ttyM0"
  KERNEL=="ttyUSB[0-9]*", KERNELS=="1-7.2:1.0", SUBSYSTEM=="tty", ACTION=="add", SYMLINK+="ttyM1"
  KERNEL=="ttyUSB[0-9]*", KERNELS=="1-7.3:1.0", SUBSYSTEM=="tty", ACTION=="add", SYMLINK+="ttyM2"
  KERNEL=="ttyUSB[0-9]*", KERNELS=="1-7.4:1.0", SUBSYSTEM=="tty", ACTION=="add", SYMLINK+="ttyM3"
  KERNEL=="ttyUSB[0-9]*", KERNELS=="1-8.1:1.0", SUBSYSTEM=="tty", ACTION=="add", SYMLINK+="ttyM4"
  KERNEL=="ttyUSB[0-9]*", KERNELS=="1-8.2:1.0", SUBSYSTEM=="tty", ACTION=="add", SYMLINK+="ttyM5"
  KERNEL=="ttyUSB[0-9]*", KERNELS=="1-8.3:1.0", SUBSYSTEM=="tty", ACTION=="add", SYMLINK+="ttyM6"
  KERNEL=="ttyUSB[0-9]*", KERNELS=="1-8.4:1.0", SUBSYSTEM=="tty", ACTION=="add", SYMLINK+="ttyM7"
  KERNEL=="ttyUSB[0-9]*", KERNELS=="1-9.1:1.0", SUBSYSTEM=="tty", ACTION=="add", SYMLINK+="ttyM8"
  KERNEL=="ttyUSB[0-9]*", KERNELS=="1-9.2:1.0", SUBSYSTEM=="tty", ACTION=="add", SYMLINK+="ttyM9"
  KERNEL=="ttyUSB[0-9]*", KERNELS=="1-9.3:1.0", SUBSYSTEM=="tty", ACTION=="add", SYMLINK+="ttyM10"
  KERNEL=="ttyUSB[0-9]*", KERNELS=="1-9.4:1.0", SUBSYSTEM=="tty", ACTION=="add", SYMLINK+="ttyM11"
  KERNEL=="ttyUSB[0-9]*", KERNELS=="1-10.1:1.0", SUBSYSTEM=="tty", ACTION=="add", SYMLINK+="ttyM12"
  KERNEL=="ttyUSB[0-9]*", KERNELS=="1-10.2:1.0", SUBSYSTEM=="tty", ACTION=="add", SYMLINK+="ttyM13"
  KERNEL=="ttyUSB[0-9]*", KERNELS=="1-10.3:1.0", SUBSYSTEM=="tty", ACTION=="add", SYMLINK+="ttyM14"
  KERNEL=="ttyUSB[0-9]*", KERNELS=="1-10.4:1.0", SUBSYSTEM=="tty", ACTION=="add", SYMLINK+="ttyM15"
  ```
  After reboot, the /dev/ttyM* device nodes are created:
  ```
  lrwxrwxrwx 1 root root 7 Sep 26 03:23 /dev/ttyM0 -> ttyUSB0
  lrwxrwxrwx 1 root root 7 Sep 26 03:23 /dev/ttyM1 -> ttyUSB1
  lrwxrwxrwx 1 root root 8 Sep 26 03:23 /dev/ttyM10 -> ttyUSB11
  lrwxrwxrwx 1 root root 8 Sep 26 03:23 /dev/ttyM11 -> ttyUSB14
  lrwxrwxrwx 1 root root 7 Sep 26 03:23 /dev/ttyM12 -> ttyUSB7
  lrwxrwxrwx 1 root root 8 Sep 26 03:23 /dev/ttyM13 -> ttyUSB10
  lrwxrwxrwx 1 root root 8 Sep 26 03:23 /dev/ttyM14 -> ttyUSB13
  lrwxrwxrwx 1 root root 8 Sep 26 03:23 /dev/ttyM15 -> ttyUSB15
  lrwxrwxrwx 1 root root 7 Sep 26 03:23 /dev/ttyM2 -> ttyUSB3
  lrwxrwxrwx 1 root root 7 Sep 26 03:23 /dev/ttyM3 -> ttyUSB6
  lrwxrwxrwx 1 root root 7 Sep 26 03:23 /dev/ttyM4 -> ttyUSB2
  lrwxrwxrwx 1 root root 7 Sep 26 03:23 /dev/ttyM5 -> ttyUSB5
  lrwxrwxrwx 1 root root 7 Sep 26 03:23 /dev/ttyM6 -> ttyUSB9
  lrwxrwxrwx 1 root root 8 Sep 26 03:23 /dev/ttyM7 -> ttyUSB12
  lrwxrwxrwx 1 root root 7 Sep 26 03:23 /dev/ttyM8 -> ttyUSB4
  lrwxrwxrwx 1 root root 7 Sep 26 03:23 /dev/ttyM9 -> ttyUSB8
  ```

### Usage
> mx-uart-ctl -p <port_number> [-m <uart_mode>]


- Get mode from port 0
 ```
 mx-uart-ctl -p 0
 ```
- Set port 1 to mode RS232
 ```
 mx-uart-ctl -p 1 -m 0
 ```
### Model IO Table
#### DA-820C
- 2 ports on the rear panel

 | Panel Port Name | Port Number | Device Node  |
 |-----------------|-------------|--------------|
 | P1              | 0           | /dev/ttyS0   |
 | P2              | 1           | /dev/ttyS1   |

- 8 ports expansion module (DN-SP08-I-DB/TB)

 | Panel Port name | Port Number | Device Node  |
 |-----------------|-------------|--------------|
 | P1              | 2           | /dev/ttyUSB0 |
 | P2              | 3           | /dev/ttyUSB1 |
 | P3              | 4           | /dev/ttyUSB2 |
 | P4              | 5           | /dev/ttyUSB3 |
 | P5              | 6           | /dev/ttyUSB4 |
 | P6              | 7           | /dev/ttyUSB5 |
 | P7              | 8           | /dev/ttyUSB6 |
 | P8              | 9           | /dev/ttyUSB7 |

#### DA-682C
- 2 ports on the rear panel

 | Panel Port Name | Port Number | Device Node  |
 |-----------------|-------------|--------------|
 | P1              | 0           | /dev/ttyS0   |
 | P2              | 1           | /dev/ttyS1   |

- When 1 UART expansion module is plugged-in (DN-SP08-I-DB/TB)

 | Panel Port name | Port Number | Device Node  |
 |-----------------|-------------|--------------|
 | P1              | 2           | /dev/ttyUSB0 |
 | P2              | 3           | /dev/ttyUSB1 |
 | P3              | 4           | /dev/ttyUSB2 |
 | P4              | 5           | /dev/ttyUSB3 |
 | P5              | 6           | /dev/ttyUSB4 |
 | P6              | 7           | /dev/ttyUSB5 |
 | P7              | 8           | /dev/ttyUSB6 |
 | P8              | 9           | /dev/ttyUSB7 |

- When 2 UART expansion modules are plugged-in (DN-SP08-I-DB/TB)

 | Panel Port name | Port Number | Device Node   |
 |-----------------|-------------|---------------|
 | P1              | 10          | /dev/ttyUSB8  |
 | P2              | 11          | /dev/ttyUSB9  |
 | P3              | 12          | /dev/ttyUSB10 |
 | P4              | 13          | /dev/ttyUSB11 |
 | P5              | 14          | /dev/ttyUSB12 |
 | P6              | 15          | /dev/ttyUSB13 |
 | P7              | 16          | /dev/ttyUSB14 |
 | P8              | 17          | /dev/ttyUSB15 |

#### DA-681C
- 12 ports on the rear panel

 | Panel Port Name | Port Number | Device Node   |
 | --------        | ----------- | ------------  |
 |    P1           | 0           | /dev/ttyUSB0  |
 |    P2           | 1           | /dev/ttyUSB1  |
 |    P3           | 2           | /dev/ttyUSB2  |
 |    P4           | 3           | /dev/ttyUSB3  |
 |    P5           | 4           | /dev/ttyUSB4  |
 |    P6           | 5           | /dev/ttyUSB5  |
 |    P7           | 6           | /dev/ttyUSB6  |
 |    P8           | 7           | /dev/ttyUSB7  |
 |    P9           | 8           | /dev/ttyUSB8  |
 |    P10          | 9           | /dev/ttyUSB9  |
 |    P11          | 10          | /dev/ttyUSB10 |
 |    P12          | 11          | /dev/ttyUSB11 |

#### V2403C
- 4 ports on the rear panel

 | Panel Port Name | Port Number | Device Node  |
 |-----------------|-------------|--------------|
 | P1              | 0           | /dev/ttyS0   |
 | P2              | 1           | /dev/ttyS1   |
 | P3              | 2           | /dev/ttyS2   |
 | P4              | 3           | /dev/ttyS3   |

#### V2201
- 2 ports on the rear panel

 | Panel Port Name | Port Number | Device Node  |
 |-----------------|-------------|--------------|
 | P1              | 0           | /dev/ttyS0   |
 | P2              | 1           | /dev/ttyS1   |

#### V3000
- 2 ports on the rear panel

 | Panel Port Name | Port Number | Device Node  |
 |-----------------|-------------|--------------|
 | P1              | 0           | /dev/ttyS0   |
 | P2              | 1           | /dev/ttyS1   |

#### EXPC-F2000
- 3 ports on the front panel

 | Panel Port Name | Port Number |  Device Node   |
 |-----------------|-------------|----------------|
 | P1              | 0           | /dev/ttyUSB1   |
 | P2              | 1           | /dev/ttyUSB2   |
 | P3              | 2           | /dev/ttyUSB3   |

#### MC-1200
- 2 ports on the rear panel

 | Panel Port Name | Port Number | Device Node  |
 |-----------------|-------------|--------------|
 | P1              | 0           | /dev/ttyS0   |
 | P2              | 1           | /dev/ttyS1   |

#### DA-680
- 8/16 ports on the rear panel

 | Panel Port Name | Port Number | Device Node   |
 | --------        | ----------- | ------------  |
 |    P1           | 0           | /dev/ttyM0    |
 |    P2           | 1           | /dev/ttyM1    |
 |    P3           | 2           | /dev/ttyM2    |
 |    P4           | 3           | /dev/ttyM3    |
 |    P5           | 4           | /dev/ttyM4    |
 |    P6           | 5           | /dev/ttyM5    |
 |    P7           | 6           | /dev/ttyM6    |
 |    P8           | 7           | /dev/ttyM7    |
 |    P9           | 8           | /dev/ttyM8    |
 |    P10          | 9           | /dev/ttyM9    |
 |    P11          | 10          | /dev/ttyM10   |
 |    P12          | 11          | /dev/ttyM11   |
 |    P13          | 12          | /dev/ttyM12   |
 |    P14          | 13          | /dev/ttyM13   |
 |    P15          | 14          | /dev/ttyM14   |
 |    P16          | 15          | /dev/ttyM15   |
