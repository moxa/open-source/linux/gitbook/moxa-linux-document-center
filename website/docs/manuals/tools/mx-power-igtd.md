---
title: mx-power-igtd
---

### Moxa Power ignition management

`mx-power-igtd` is a common script  the controller that manages power distribution to\
in-vehicle computers by receiving signals when starting the ignition system of\
transportation vehicles.

### Source Code Link
[Download](https://gitlab.com/moxa/open-source/linux/packages/moxa-example-code/-/blob/main/x86/POWER_IGT)

### Support Model

- V Series:
  - [V2403C](#v2403c)

### Prerequisite
Before use `mx-power-igtd` script, you should install drivers as below.
1. [moxa-it87-gpio-driver](../drivers/moxa-it87-gpio-driver.md)

### Usage
- Install
```
$ cp mx-power-igtd /usr/sbin/
$ cp moxa-power-igtd.service /etc/systemd/system
```
- Run the service
1. Reload the service files to include the new service.
```
$ systemctl daemon-reload
```
2. Start service
```
$ systemctl start moxa-power-igtd.service
```
3. To check the status of service
```
$ systemctl status moxa-power-igtd.service
● moxa-power-igtd.service - Moxa power ignition daemon service
     Loaded: loaded (/etc/systemd/system/moxa-power-igtd.service; enabled; vendor preset: enabled)
     Active: active (running) since Mon 2022-06-20 22:20:00 PDT; 1h 12min ago
   Main PID: 473 (mx-power-igtd)
      Tasks: 2 (limit: 9347)
     Memory: 2.9M
        CPU: 36.482s
     CGroup: /system.slice/moxa-power-igtd.service
             ├─ 473 /bin/bash /usr/sbin/mx-power-igtd
             └─9415 sleep 1

Jun 20 22:20:00 moxa systemd[1]: Started Moxa power ignition daemon service.
```
4. To enable service on every reboot
```
$ systemctl enable moxa-power-igtd.service
```