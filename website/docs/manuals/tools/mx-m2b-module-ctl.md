---
title: mx-m2b-module-ctl
---

### Moxa M.2 B Key Module Control
`mx-m2b-module-ctl` is a common script for controlling M.2 B key module.

### Source Code Link
[Download](https://gitlab.com/moxa/open-source/linux/packages/moxa-example-code/-/blob/main/x86/MODULE/mx-m2b-module-ctl)

### Support Model
- V Series:
  - [V3000](#v3000)

### Prerequisite
Before use `mx-m2b-module-ctl` script, you should install drivers as below.
1. [moxa-gpio-pca953x-driver](../drivers/moxa-gpio-pca953x-driver.md)

### Usage
```
Usage:
       mx-m2b-module-ctl [Options]

Operations:
       -s, --slot <module_slot_id>
               Select module slot
       -p, --pwren [high|low]
               Get/Set power enable pin high/low status
       -t, --turnon [high|low]
               Get/Set turn-high pin high/low status
       -r, --reset [high|low]
               Get/Set reset pin high/low status
       -a, --airplane [high|low]
               Get/Set airplane pin high/low status
       -i, --sim 1|2
               Get/Set sim card slot 1/2
       -m, --mod_status 1|2
               Get module and config status

Example:
       Set power enable to [high] status for module 1
       # mx-m2b-module-ctl -s 1 -p high
       Get power enable pin status of module 1
       # mx-m2b-module-ctl -s 1 -p

       Set turn-on pin to [high] status for module 1
       # mx-m2b-module-ctl -s 1 -t high
       Get turn-on pin status of module 1
       # mx-m2b-module-ctl -s 1 -t

       Set reset pin to [low] status for module 2
       # mx-m2b-module-ctl -s 2 -r low
       Get reset pin status of module 2
       # mx-m2b-module-ctl -s 2 -r

       Set airplane pin to [low] status for module 2
       # mx-m2b-module-ctl -s 2 -a low
       Get airplane pin status of module 2
       # mx-m2b-module-ctl -s 2 -a

       Select SIM slot 2 for module 2
       # mx-m2b-module-ctl -s 2 -i 2
       Get current SIM slot of module 2
       # mx-m2b-module-ctl -s 2 -i

       Get module 2 status
       # mx-m2b-module-ctl -s 2 -m
```

- Pull power enable high on module 1
 ```
 mx-m2b-module-ctl -s 1 -p on
 ```
- Set module 1 reset pin to high
 ```
 mx-m2b-module-ctl -s 1 -r high
 ```
- Set module 2 turn-on pin to high
 ```
 mx-m2b-module-ctl -s 2 -t high
 ```
- Set module 2 airplane pin to high
 ```
 mx-m2b-module-ctl -s 2 -a high

- Select SIM 2 for module 1
 ```
 mx-m2b-module-ctl -s 1 -i 2
 ```
- Get power status of module 1
 ```
 mx-m2b-module-ctl -s 1 -p
 ```
- Get current SIM slot of module 1
 ```
 mx-m2b-module-ctl -s 1 -i
 ```

### Model IO Table
#### V3000
- Power Control

 | Slot ID  |
 |----------|
 | #1       |
 | #2       |

- Sim card slots

 | Sim Slot ID  |
 |--------------|
 | #1           |
 | #2           |
