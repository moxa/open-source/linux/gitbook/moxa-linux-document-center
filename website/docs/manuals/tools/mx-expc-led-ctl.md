---
title: mx-expc-led-ctl
---

### Moxa EXPC LED Controll

`mx-expc-led-ctl` is a common script for controlling LED's interface mode on EXPC-F2000 series

### Source Code Link
[Download](https://gitlab.com/moxa/open-source/linux/packages/moxa-example-code/-/blob/main/x86/LED/mx-expc-led-ctl)

### Support Model

- EXPC Series:
  - [EXPC-F2000](#expc-f2000)

### Prerequisite
Before use `mx-expc-led-ctl` script, you should install drivers as below.
1. [moxa-it87-gpio-driver](../drivers/moxa-it87-gpio-driver.md)
2. Install `efivar` package: `apt update && apt install efivar -y`


### Usage

```
Usage:
        mx-expc-led-ctl -i <led_index> [LAN1|LAN2|LAN3|LAN4|UART1_TX|UART2_TX|UART3_TX|UART1_RX|UART2_RX|UART3_RX]

OPTIONS:
        -i <led_index>
                Set LED index.

Example:
        Get mode from index 0
        # mx-expc-led-ctl -i 0

        Set LED index 0 to LAN1 mode
        # mx-expc-led-ctl -i 0 LAN1

        Set LED index 1 to UART3_TX mode
        # mx-expc-led-ctl -i 1 UART3_TX
```

### Model IO Table
#### EXPC-F2000
- 2 programmable LEDs on the display panel
