---
title: moxa-audio-retask
---

### Moxa Audio Retask

`moxa-audio-retask` is a common script for soundcards retasking audio jack.

### Source Code Link
[Download](https://gitlab.com/moxa/open-source/linux/packages/moxa-example-code/-/blob/main/x86/AUDIO_RETASK/moxa-audio-retask)

### Support Model

- V Series:
  - [V2403C](#v2403c)
  - [V2406C](#v2406c)
- MC Series
  - [MC-7400](#mc7400)

### Usage
> moxa-audio-retask -p <port_index> [-m <jack_mode>]


- Get Jack 1 mode
 ```
 moxa-audio-retask -p 1
 ```
- Set Jack 2 to HEADPHONE mode
 ```
 moxa-audio-retask -p 2 -m 2
 ```

:::caution
Line out audio port only support **output modes**.

Line in audio port: support **output modes** & **input modes**.
:::

### Desktop Environment
If you use GNOME, KDE, etc. as your Linux desktop manager, it is recommended to use GUI tools as below.
```
apt-get update && apt-get install alsa-tools-gui hdajackretask
```

- Reference
  - [For Debian](https://packages.debian.org/bullseye/alsa-tools-gui)
  - [For Ubuntu](https://packages.ubuntu.com/focal/alsa-tools-gui)


### Model IO Table
#### V2403C
- 2 audio jacks on the rear panel

#### V2406C
- 2 audio jacks on the rear panel

#### MC7400
- 2 audio jacks on the rear panel