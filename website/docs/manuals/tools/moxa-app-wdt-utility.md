---
title: moxa-app-wdt-utility
---

### Moxa app watchdog utility

The **Moxa app watchdog utility** is an utility for controlling MCU's app watchdog mode

### Source Code Link
[moxa-app-wdt-utility](https://gitlab.com/moxa/open-source/linux/packages/moxa-app-wdt-utility/-/tree/master)

### Support Model

- V Series:
  - [V3000](../products/V/V3000.md)

### Prerequisite
Before use `moxa-app-wdt-utility`, you should build source code and add systemd service:

- Build source code

```
apt update && apt install build-essential
git clone https://gitlab.com/moxa/open-source/linux/packages/moxa-app-wdt-utility.git -b master
cd moxa-app-wdt-utility
make
make install
```

- Add and enable systemd service for acking app watchdog

```
cp mx-app-wdtd.service /lib/systemd/system/
systemctl enable mx-app-wdtd.service

# start and check service status
systemctl start mx-app-wdtd.service
systemctl status mx-app-wdtd.service

# to disble app-wdt when reboot
cp mx-disable-app-wdt.service /lib/systemd/system/
systemctl enable mx-app-wdtd.service
systemctl status mx-app-wdtd.service
```

the `mx-app-wdtd` service status shall be like:
```
● mx-app-wdtd.service - Moxa app watchdog daemon service
     Loaded: loaded (/lib/systemd/system/mx-app-wdtd.service; enabled; vendor preset: enabled)
     Active: active (running) since Wed 2022-06-29 02:39:03 EDT; 38min ago
   Main PID: 5463 (mx-app-wdtd)
      Tasks: 1 (limit: 9236)
     Memory: 148.0K
        CPU: 134ms
     CGroup: /system.slice/mx-app-wdtd.service
             └─5463 /usr/sbin/mx-app-wdtd

Jun 29 02:39:03 debian systemd[1]: Started Moxa app watchdog daemon service.
```

### Usage

```
Usage:
        mx-app-wdt-ctl [Options]...
Options:
        -a, --appwdt_mode
                Get app wdt mode and timeout status
        -a, --appwdt_mode [0|1] [5:15]
                Set app wdt mode and timeout [0:disable|1:enable] [5~15 timeout in sec]
        -w, --appwdt_resetmode
                Get app wdt reset mode
        -w, --appwdt_resetmode [0|1]
                Set app wdt reset mode (0:not reset|1:reset)
        -m, --appwdt_relaymode
                Get app wdt relay mode
        -m, --appwdt_relaymode [0|1|2]
                Set app wdt relay mode (0:connect|1:disconnect|2:by-pass)
```

:::caution
Moxa APP Watchdog Service will occupy the MCU ommunication port,  
please stop the APP Watchdog service before using the MCU control tools. (e.g. LAN bypass, MCU upgrade tool or app watchdog control)
:::

For exmaple:
```
# Stop Moxa APP Watchdog Service
systemctl stop mx-app-wdtd.service

# Check communication port status (if needed)
lsof /dev/ttyS2
```
