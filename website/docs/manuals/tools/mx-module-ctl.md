---
title: mx-module-ctl
---

### Moxa Module Control
`mx-module-ctl` is a common script for controlling Module.

### Source Code Link
[Download](https://gitlab.com/moxa/open-source/linux/packages/moxa-example-code/-/blob/main/x86/MODULE/mx-module-ctl)

### Support Model
- V Series:
  - [V2403C](#v2403c)
  - [V2406C](#v2406c)
  - [V2201](#v2201)
  - [V3000](#v3000)

### Prerequisite
Before use `mx-module-ctl` script, you should install drivers as below.
1. [moxa-it87-gpio-driver](../drivers/moxa-it87-gpio-driver.md)

For **V3000** series model, only to use **pca953x** driver to control module:
1. [moxa-gpio-pca953x-driver](../drivers/moxa-gpio-pca953x-driver.md)

### Usage
```
Usage:
       mx-module-ctl [Options]

Operations:
       -s, --slot <module_slot_id>
               Select module slot
       -p, --power [on|off]
               Get/Set power on/off module
       -r, --reset [on|off]
               Get/Set reset pin to high(on)/low(off) to slot
       -i, --sim 1|2
               Get/Set sim card slot

Example:
       Power on module 1
       # mx-module-ctl -s 1 -p on

       Set module 2 reset pin to high
       # mx-module-ctl -s 2 -r on

       Select SIM 2 for module 1
       # mx-module-ctl -s 1 -i 2

       Get power status of module 1
       # mx-module-ctl -s 1 -p

       Get current SIM slot of module 1
       # mx-module-ctl -s 1 -i
```

- Power on module 1
 ```
 mx-module-ctl -s 1 -p on
 ```
- Set module 2 reset pin to high
 ```
 mx-module-ctl -s 2 -r on
 ```
- Select SIM 2 for module 1
 ```
 mx-module-ctl -s 1 -i 2
 ```
- Get power status of module 1
 ```
 mx-module-ctl -s 1 -p
 ```
- Get current SIM slot of module 1
 ```
 mx-module-ctl -s 1 -i
 ```

### Model IO Table
#### V2403C
- mPCIe Power Control

 | Slot ID  |
 |------|
 | #1   |
 | #2   |

- Sim card slot in the front panel

 | Sim Slot ID  |
 |------|
 | #1   |
 | #2   |

#### V2403C
- mPCIe Power Control

 | Slot ID  |
 |------|
 | #1   |
 | #2   |

- Sim card slot in the front panel

 | Sim Slot ID  |
 |------|
 | #1   |
 | #2   |

#### V2403C
- mPCIe Power Control

 | Slot ID  |
 |------|
 | #1   |

#### V3000
- mPCIe Power Control

 | Slot ID  |
 |----------|
 | #1       |

- Sim card slots

 | Sim Slot ID  |
 |--------------|
 | #1           |
