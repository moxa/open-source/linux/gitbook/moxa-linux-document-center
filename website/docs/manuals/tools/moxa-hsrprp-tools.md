---
title: moxa-hsrprp-tools
---

### Moxa HSR/PRP card utility

`Moxa HSR/PRP card utility` is based on SMBUS to query FPGA related register.

### Source Code Link
[moxa-hsrprp-tools](https://gitlab.com/moxa/open-source/linux/packages/moxa-hsrprp-tools/-/tree/master)

### Support Model

- DA Series:
  - [DA-820C](../products/DA/DA-820C.md)
  - [DA-682C](../products/DA/DA-682C.md)
  - [DA-720](../products/DA/DA-720.md)

### Prerequisite
Before use `moxa-hsrprp-tools`, you should build source code and add systemd service:

- Build source code

```
apt update && apt install build-essential
git clone https://gitlab.com/moxa/open-source/linux/packages/moxa-hsrprp-tools.git -b master
cd moxa-hsrprp-tools
make
make install
```
- Add systemd service (if needed)

edit `/lib/systemd/system/mx_hsrprp.service`
```
[Unit]
Description=Moxa HSR-PRP daemon service

[Service]
Type=oneshot
ExecStart=/usr/sbin/mx_hsrprp start
ExecStop=/usr/sbin/mx_hsrprp stop
RemainAfterExit=yes

[Install]
WantedBy=multi-user.target
```

then enable service.
```
systemctl enable mx_hsrprp.service
```

### Usage

```
The time sync daemon default configure wtih
  -B - Run daemon in the background
  -t: HSR/PRP Status update period. Unit per second.
  -b: SMBUS device, default is /dev/i2c-0
  -m: configure to prp or hsr mode, default is prp mode.
      The argurement is [index]:[mode]
      [index] range from 0~7.
      [mode] 0 is prp, mode 1 is hsr.
      Ex: Set card 0 to hsr mode, card 1 to prp mode.
      root@Moxa:~# mxhsrprpd -t 2 -m 0:1,1:0
  -s: configure fiber speed, default is auto detect mode.
      The argurement is [index]:[speed]
      [index] range from 0~7.
      [speed] 0 is auto-detect, speed 1 is 100M, speed 2 is 1000M.
      Ex: Set card 0 fiber to 100M, card 1 fiber to 1000M.
      root@Moxa:~# mxhsrprpd -t 2 -s 0:1,1:2
```
