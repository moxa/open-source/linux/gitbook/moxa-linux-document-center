---
title: moxa-irigb-tools
---

### Moxa IRIG-B Tools

Utility for controlling IRIG-B expansion module, compile and install the IRIG-B time sync daemon

### Source Code Link
[moxa-irigb-tools](https://gitlab.com/moxa/open-source/linux/packages/moxa-irigb-tools/-/tree/master)

### Support Model

- DA Series:
  - [DA-820C](../products/DA/DA-820C.md)
  - [DA-720](../products/DA/DA-720.md)
  - [DA-680](../products/DA/DA-680.md)

### Prerequisite
Before use `moxa-irigb-tools`, you should install `moxa-irigb-driver` driver as below.
- [moxa-irigb-driver](../drivers/moxa-irigb-driver.md)

### Usage
#### 1. Disable NTP service

- Disable service
```
timedatectl set-ntp false
```
:::caution
NTP service affects IRIG-B service time syncing.
:::
- Make sure NTP service is inactive
```
timedatectl status
               Local time: Mon 2023-02-13 02:27:54 PST
           Universal time: Mon 2023-02-13 10:27:54 UTC
                 RTC time: Mon 2023-02-13 10:27:54
                Time zone: America/Los_Angeles (PST, -0800)
System clock synchronized: yes
              NTP service: inactive
          RTC in local TZ: no
```

#### 2. Build and install IRIG-B time sync service
- Install build tools (Ubuntu/Debian)
```
apt update && apt install build-essential -y
```
- Install build tools (CentOS)
```
yum groupinstall "Development Tools"
```
- Build and install
```
make && make install
```
#### 3. Config IRIG-B time sync service
- Edit `/usr/sbin/mx_irigb.sh` to config service options `MX_IRIGB_SERVICESYNCTIME_OPTS`.
:::info
For more detial about options, please refer to `ServiceSyncTime -h`
:::

```
...
# The time sync daemon default configure wtih
#   -t 1 - Sync time in DIFF signal format
#   -i 10 - The time interval in 10 seconds to sync the IRIG-B time into system time.
#   -B - Run daemon in the background
#
MX_IRIGB_SERVICESYNCTIME_OPTS="-t 1 -i 10 -B"
...
```
#### 4. Start IRIG-B time sync service
- Create and edit systemd service file `/lib/systemd/system/mx_irigb.service`
```
[Unit]
Description=Moxa DA-IRIG-B daemon service

[Service]
Type=oneshot
ExecStart=/usr/sbin/mx_irigb.sh start
ExecStop=/usr/sbin/mx_irigb.sh stop
RemainAfterExit=yes

[Install]
WantedBy=multi-user.target
```

- Launch service
```
$ systemctl daemon-reload
$ systemctl enable mx_irigb.service
Created symlink /etc/systemd/system/multi-user.target.wants/mx_irigb.service → /lib/systemd/system/mx_irigb.service.
$ systemctl start mx_irigb.service
$ systemctl status mx_irigb.service
● mx_irigb.service - Moxa DA-IRIG-B daemon service
     Loaded: loaded (/lib/systemd/system/mx_irigb.service; enabled; vendor preset: enabled)
     Active: active (exited) since Tue 2023-02-14 01:48:29 PST; 5s ago
    Process: 8322 ExecStart=/usr/sbin/mx_irigb.sh start (code=exited, status=0/SUCCESS)
   Main PID: 8322 (code=exited, status=0/SUCCESS)
        CPU: 9ms

Feb 14 01:48:29 moxa systemd[1]: Starting Moxa DA-IRIG-B daemon service...
Feb 14 01:48:29 moxa systemd[1]: Finished Moxa DA-IRIG-B daemon service.
```