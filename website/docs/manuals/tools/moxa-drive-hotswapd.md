---
title: moxa-drive-hotswapd
---

### Moxa HDD/SSD drive hotswap daemon

`mxhtspd` is a daemon that monitor hot-swappable disk buttons event.

### Source Code Link
[Download](https://gitlab.com/moxa/open-source/linux/packages/moxa-drive-hotswapd)

### Support Model

- V Series:
  - [V2406C](#v2406c)

### Prerequisite
Before use `moxa-hotswap-driver` script, you should install drivers as below.
1. [moxa-hotswap-driver](../drivers/moxa-hotswap-driver.md)

### Usage
- Build
  ```
  make && make install
  ```
- Start service
  ```
  systemctl daemon-reload
  systemctl enable mx_hotswapd.service
  ```
- To check the status of service
  ```
  systemctl status mx_hotswapd.service
  ```
### Tested with SSD/HDDs

- When SSD/HDDs were inserted to two slots (for example: Disk 1), the terminals will show message to info user.
  :::tip
  kernel > 4.15 needs to press Disk 1/2 button under 3 seconds to detect disk.
  :::
  ```
  mxhtspd: Disk slot 1 was detected.

  root@Moxa:/home/moxa# mount | grep "/media"
  /dev/sdb1 on /media/disk1p1 type ext4 (rw,relatime,data=ordered)
  /dev/sdc1 on /media/disk2p1 type ext4 (rw,relatime,data=ordered)
  ```

- Press Disk 1/2 button over 3 seconds, the terminals will show message to info user to remove disk.
  ```
  mxhtspd: Remove disk 1 is done.

  root@Moxa:/home/moxa# mount | grep "/media"
  /dev/sdb1 on /media/disk1p1 type ext4 (rw,relatime,data=ordered)
  /dev/sdc1 on /media/disk2p1 type ext4 (rw,relatime,data=ordered
  ```

### Model IO Table
#### V2406C
- 2 hot-swappable disk buttons on the front view.