---
title: IRIGB
---

The 'DA-IRIGB-B-S', 'DE-2-IRIGB-4-DI/DO' module features 3 digital inputs and 4 digital outputs and provides precision timing information using IRIG-B input signals. The module is designed for embedded computers that support the PCI/104 interface.

## Datasheet

- [DA-IRIGB-B-S Website](https://www.moxa.com/en/products/industrial-computing/x86-computers/da-irigb-b-s-series-expansion-modules)
- [moxa-da-irigb-b-s-series-expansion-modules-datasheet-v1.0.pdf](https://cdn-cms.azureedge.net/getmedia/334280ed-57cd-45af-b6e7-b6d913be5a58/moxa-da-irigb-b-s-series-expansion-modules-datasheet-v1.0.pdf)

## Prerequisite

- See [Introduction](../../intro.md) page for more informations.
- See [moxa-irigb-tools](../../tools/moxa-irigb-tools.md) for Moxa IRIG-B Tools
- See [moxa-irigb-driver](../../drivers/moxa-irigb-driver.md) for Moxa IRIG-B Driver

## Support Model

- DA Series:
  - DA-IRIGB-B-S: DA-820C
  - DE-2-IRIGB-4-DI/DO: DA-720
