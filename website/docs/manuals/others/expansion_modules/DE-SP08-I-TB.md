---
title: DE-SP08-I-TB
---

## Datasheet

### DE-SP08-I-TB

The DA-720-UART Series expansion modules include an 8-port RS-232/422/485 module with terminal block connectors. The modules are designed
with 2 kV digital isolation, making them fully suitable for the demands of industrial applications that use serial communication.

- [DE-SP08-I-TB Website](https://www.moxa.com/en/products/industrial-computing/x86-computers/da-720-uart-series-expansion-modules/de-sp08-i-tb)
- [moxa-da-720-uart-series-expansion-modules-datasheet-v1.0.pdf](https://www.moxa.com/getmedia/fe9dd6c1-db3b-4ccb-98db-3feba799172a/moxa-da-720-uart-series-expansion-modules-datasheet-v1.0.pdf)

## Support Model

- DA Series:
  - [DA-720](../../products/DA/DA-720.md)

## Prerequisite
- See [Introduction](../../intro.md) page for more informations.

## UART Driver
- drivers download link:
  - For Debian 11, Ubuntu 20.04: [moxa-msb-linux-kernel-5.x.x-driver-v5.1.tgz](https://cdn-cms.azureedge.net/getmedia/411710e8-320f-42ac-970b-0aa46a04c457/moxa-msb-linux-kernel-5.x.x-driver-v5.1.tgz)
  - For CentOS 7.9: [moxa-linux-kernel-3.x.x-driver-v3.0.tgz](https://cdn-cms.azureedge.net/getmedia/ad349058-9e70-4838-8a75-3b339782880d/moxa-linux-kernel-3.x.x-driver-v3.0.tgz)
  - Or reference [Moxa webpage](https://www.moxa.com/en/products/industrial-edge-connectivity/multiport-serial-boards/pcie-upci-pci-serial-cards/cp-168u-series#resources)

- before build drivers on Linux host
```
# according to host's Linux distribution to install kernel headers and necessary for compiling software
## for Debian/Ubuntu
apt update && apt install build-essential linux-headers-`uname -r`

## for CentOS 7.9
yum distro-sync
yum install "kernel-devel-$(uname -r)"
yum install "kernel-headers-$(uname -r)"
yum install gcc
```

- build drivers (Debian 11, Ubuntu 20.04)
```
# build and install drivers
wget https://cdn-cms.azureedge.net/getmedia/411710e8-320f-42ac-970b-0aa46a04c457/moxa-msb-linux-kernel-5.x.x-driver-v5.1.tgz

tar xvf moxa-msb-linux-kernel-5.x.x-driver-v5.1.tgz
cd mxser/
./mxinstall
```

- build drivers (CentOS 7.9)
```
# build and install drivers
wget https://cdn-cms.azureedge.net/getmedia/ad349058-9e70-4838-8a75-3b339782880d/moxa-linux-kernel-3.x.x-driver-v3.0.tgz

tar xvf moxa-linux-kernel-3.x.x-driver-v3.0.tgz
cd mxser/
./mxinstall
```

- then check the DE-SP08-I-TB UART interfaces are created by `mxser` driver:
```
 dmesg | grep mxser
[    3.630964] mxser: MOXA Smartio/Industio family driver version 5.1
[    3.631021] mxser: Found MOXA CP-118U series board(BusNo=30,DevNo=8)
[    3.631210] mxser:         ttyM0 - ttyM7
[    3.631211] mxser:  max. baud rate = 921600 bps.
```

## Setinterface Utility
- Build and install [moxa-misc-driver](../../drivers/moxa-misc-driver.md) on DA-720 host
- setinterface utility repository: https://gitlab.com/moxa/open-source/linux/packages/moxa-example-code
- clone code and build utility
```
wget https://gitlab.com/moxa/open-source/linux/packages/moxa-example-code/-/archive/main/moxa-example-code-main.tar.gz
tar xvf moxa-example-code-main.tar.gz
cd moxa-example-code-main/x86/UART/setinterface
make && make install
```

- control DE-SP08-I-TB UART interfaces mode (P1 ~ P8)

```
# get UART mode at P1
setinterface /dev/ttyM0

# set as RS232 mode at P1
setinterface /dev/ttyM0 0

# set as RS485-2WIRES mode at P1
setinterface /dev/ttyM0 1

# set as RS422 mode at P1
setinterface /dev/ttyM0 2

# ...

# get UART mode at P8
setinterface /dev/ttyM7

# set as RS232 mode at P8
setinterface /dev/ttyM7 0

# set as RS485-2WIRES mode at P8
setinterface /dev/ttyM7 1

# set as RS422 mode at P1
setinterface /dev/ttyM7 2
```
