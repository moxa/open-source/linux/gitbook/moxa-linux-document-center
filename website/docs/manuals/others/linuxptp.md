---
title: Linux PTP
---

## Support LAN chip models
- Intel I210 (driver ibg)
- Intel I219 (driver e1000e)

## Support Moxa models
- DA Series: 
  - DA-820C (LAN 1 - 4)
  - DA-682C (LAN 1 - 6)
  - DA-681C (LAN 1 - 6)
  - DA-720 (LAN 1 - 6)

## Debian Linuxptp package

[https://packages.debian.org/bullseye/linuxptp](https://packages.debian.org/bullseye/linuxptp)

- Install Debian 11
- Install packages

```
apt update && apt install linuxptp
```

- Disable systemd time sync daemon service to avoid some unexpected operations

```
systemctl stop systemd-timesyncd
systemctl disable systemd-timesyncd
```

Linuxptp is an implementation of the Precision Time Protocol (PTP) according to IEEE standard 1588 for Linux.
Features include:
  - support for hardware and software time stamping via the Linux
    `SO_TIMESTAMPING` socket option.
  - support for the `Linux PTP Hardware Clock (PHC)` subsystem by using the
    `clock_gettime` family of calls, including the new clock_adjtimex
    system call
  - implementation of Boundary Clock (BC) and Ordinary Clock (OC)
  - transport over UDP/IPv4, UDP/IPv6, and raw Ethernet (Layer 2)
  - support for IEEE 802.1AS-2011 in the role of end station

PTP provides higher precision and faster synchronization than NTP even without hardware support. With hardware support, sub-microsecond accuracy can be expected. Whereas NTP is intended for WAN use, PTP is designed for LAN environments and makes use of UDP multicast.

```
usage: ptp4l [options]

 Delay Mechanism

 -A        Auto, starting with E2E
 -E        E2E, delay request-response (default)
 -P        P2P, peer delay mechanism

 Network Transport

 -2        IEEE 802.3
 -4        UDP IPV4 (default)
 -6        UDP IPV6

 Time Stamping

 -H        HARDWARE (default)
 -S        SOFTWARE
 -L        LEGACY HW

 Other Options

 -f [file] read configuration from 'file'
 -i [dev]  interface device to use, for example 'eth0'
           (may be specified multiple times)
 -p [dev]  Clock device to use, default auto
           (ignored for SOFTWARE/LEGACY HW time stamping)
 -s        slave only mode (overrides configuration file)
 -l [num]  set the logging level to 'num'
 -m        print messages to stdout
 -q        do not print messages to the syslog
 -v        prints the software version and exits
 -h        prints this message and exits
```

## Debian phc2sys package

`phc2sys` is a program which synchronizes two or more clocks in the system. Typically, it is used to synchronize the system clock to a PTP hardware clock (PHC), which itself is synchronized by the ptp4l(8) program.

[https://manpages.debian.org/bullseye/linuxptp/phc2sys.8.en.html](https://manpages.debian.org/bullseye/linuxptp/phc2sys.8.en.html)

```
phc2sys -a -m -r -R 10
```

## PTP modes
### OC mode

- Set as OC master mode
    - Layer 2
    - P2P mode, peer delay mechanism

```
# Assume interface device is 'enp4s0'
ip link set dev enp4s0 up
ptp4l -m -2 -P -i enp4s0
```

- Set as OC slave mode
    - Layer 2
    - P2P mode, peer delay mechanism

```
# Assume interface device is 'enp5s0'
ip link set dev enp5s0 up
ptp4l -m -2 -P -s -i enp5s0
# with log: ptp4l -m -2 -s -P -i enp5s0 2>&1 | tee $(date +%Y%m%d%H%M%S.log)

# use phc2sys to sync sys clock for 10Hz
phc2sys -a -m -r -R 10
```

### BC mode

- Set as BC mode host
    - clock_type
    Specifies the kind of PTP clock.  Valid values are "OC" for ordinary clock, "BC" for boundary clock, "P2P_TC" for peer to peer transparent clock, and "E2E_TC" for end to end transparent clock. An multi-port ordinary clock will automatically be configured as a boundary clock.  The default is "OC".
    - boundary_clock_jbod
    When running as a **boundary clock** (that is, when more than one network interface is configured), ptp4l performs a sanity check to make sure that all of the ports share the same hardware clock device. This option allows ptp4l to work as a boundary clock using "just a bunch of  devices"  that are not synchronized to each other. For this mode, the collection of clocks must be synchronized by an external program, for example phc2sys(8) in "automatic" mode.  The default is 0 (disabled).

```
# For example, edit config file 'bc.cfg'
# and assume 'enp12s0' and 'enp4s0' are connected network interface
[global]
sanity_freq_limit        0
step_threshold           0.000002
tx_timestamp_timeout     10
logMinPdelayReqInterval  0
logSyncInterval          0
logAnnounceInterval      0
announceReceiptTimeout   3
syncReceiptTimeout       2
twoStepFlag              1
summary_interval         0
clock_type               BC
priority1                128
priority2                127
delay_mechanism          P2P

[enp12s0]
boundary_clock_jbod          1
network_transport            L2
fault_reset_interval         0

[enp4s0]
boundary_clock_jbod          1
network_transport            L2
fault_reset_interval         0

# run the ptp4l procedure
ip link set dev enp12s0 up
ip link set dev enp4s0 up
ptp4l -m -f bc.cfg

# use phc2sys to sync sys clock for 10Hz
phc2sys -a -m -r -R 10
```

- On OC Grandmaster
```
# assume interface is enp5s0
ip link set dev enp5s0 up
ptp4l -2 -m -P -i enp5s0
```

- On OC Slave
```
# assume interface is enp4s0
ip link set dev enp4s0 up
ptp4l -2 -m -s -P -i enp4s0

# with log: ptp4l -2 -m -s -P -i enp4s0 2>&1 | tee $(date +%Y%m%d%H%M%S.log)
```

### TC mode

- Set TC mode host:
```
# For example, edit config file 'tc.cfg'
# and assume 'enp12s0' and 'enp4s0' are connected network interface
[global]
priority1               254
priority2               253
free_running            1
freq_est_interval       3
tc_spanning_tree        1
clock_type              P2P_TC
network_transport       L2
delay_mechanism         P2P

[enp12s0]
egressLatency           0
ingressLatency          0
delay_mechanism         P2P
network_transport       L2

[enp4s0]
egressLatency           0
ingressLatency          0
delay_mechanism         P2P
network_transport       L2

# run the ptp4l procedure
ip link set dev enp12s0 up
ip link set dev enp4s0 up
ptp4l -m -f tc.cfg

# use phc2sys to sync sys clock between master & slave for 10Hz
# -c Specify the slave clock by device (e.g. /dev/ptp1) or interface (e.g. eth1)
# -s Specify the master clock by device (e.g. /dev/ptp0) or interface (e.g. eth0)
phc2sys -s enp12s0 -c enp4s0 -O 0 -R 10 -m
```

- As OC Grandmaster
```bash
# assume interface is enp5s0
ip link set dev enp5s0 up
ptp4l -2 -m -P -i enp5s0
```

- As OC Slave
```
# assume interface is enp4s0
ip link set dev enp4s0 up
ptp4l -2 -m -s -P -i enp4s0

# use phc2sys to sync sys clock for 10Hz on slve
phc2sys -a -m -r -R 10
```
